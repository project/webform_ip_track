
INTRODUCTION:
------------
This module provides feature for sending location details,
through email, when a webform is submitted.

For a full description of the module, visit the project page:

https://www.drupal.org/project/webform_ip_track

REQUIREMENTS:
-------------
This module requires the following modules:

tokens

INSTALLATION:
-------------
1. Place the entire webform ip track folder into your Drupal sites/all/modules/
   directory.

2. Enable the webform ip track module.

CONFIGURATION:
-------------

1. Once the module is installed,
   you will find a new token type named Location in token tree.

2. Use these token fields in the webform submission values which
   will be parsed and location details will be sent in the email.

Author:
-------
Jack Ry
